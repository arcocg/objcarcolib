//
//  DAOBase.m
//  WeatherBar
//
//  Created by Apple Apple on 10.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ARCODAOBase.h"

@implementation ARCODAOBase

- (id)initWithDBPath:(NSString*)indbpath {
    
    self = [super init];
    if (self) {
        dbpath = indbpath;
    }
    return self;
}

- (BOOL) openDB
{
    if (dbpath!=nil) {
      [self openDB:dbpath];
        return true;
    }
    else return false;
}

- (BOOL) openDB:(NSString*)inpath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:inpath];
    
    
    if(!success)
    {
        NSLog(@"[DAOBase] openDB: Cannot locate database file '%@'.", inpath);
        return false;
    }
    if(!(sqlite3_open([dbpath UTF8String], &db) == SQLITE_OK))
    {
        NSLog(@"[DAOBase] openDB: An error has occured.");
        return false;
    }
    else return true;
}

- (void) closeDB
{
    if (db) {
        sqlite3_close(db);
        db = nil;
    }  
}

-(void)dealloc{
    [self closeDB];
}
@end
