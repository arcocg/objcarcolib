//
//  DAOBase.h
//  WeatherBar
//
//  Created by Apple Apple on 10.02.12.
//  Copyright (c) 2012 arcomailbo@gmail.com. All rights reserved.
//

#import <sqlite3.h>
#import <Foundation/Foundation.h>

@interface ARCODAOBase : NSObject{
    sqlite3 *db; //Database
    NSString* dbpath;
}
    - (id)initWithDBPath:(NSString*)indbpath;
    - (BOOL) openDB;
    - (BOOL) openDB:(NSString*)inpath;
    - (void) closeDB; 
@end
