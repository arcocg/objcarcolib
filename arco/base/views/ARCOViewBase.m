//
//  ViewBase.m
//  WeatherBar
//
//  Created by Apple Apple on 07.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ARCOViewBase.h"

@implementation ARCOViewBase
@synthesize model = _model;



-(void)addEventListener:(NSString *)inevent selector:(SEL)inselector{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:inselector name:inevent object:nil];
}

-(void)removeEventListener:(NSString *)inevent{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:inevent object:nil];
}

-(void)buildWithModel:(ModelMain *)inmodel{
    _model = inmodel;
}


-(id)initWithModel:(ModelMain *)inmodel{
    self = [super init];
    if (self) {
        // Initialization code
        _model = inmodel;
    }
    return self;
    
}

-(id)initWithModel:(ModelMain *)inmodel frame:(CGRect)inframe{
    self = [super initWithFrame:inframe];
    if (self) {
        // Initialization code
        _model = inmodel;
    }
    return self;
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) reorderToPortrait:(CGRect)inframe {}
- (void) reorderToLandscape:(CGRect)inframe {}
- (void) changeLayout:(CGRect)inframe {}


@end
