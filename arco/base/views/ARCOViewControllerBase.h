//
//  ViewControllerBase.h
//  WeatherBar
//
//  Created by Apple Apple on 13.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@class AppDelegate;
@class ModelMain;

#import <UIKit/UIKit.h>

@interface ARCOViewControllerBase : UIViewController{
    ModelMain *model;
}



@property (nonatomic, strong) ModelMain *model;

- (void) addEventListener:(NSString*)inevent selector:(SEL)inselector;
- (void) removeEventListener:(NSString*)inevent;
/*- (void) onModelInited;*/
@end
