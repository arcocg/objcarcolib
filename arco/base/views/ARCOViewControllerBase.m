//
//  ViewControllerBase.m
//  WeatherBar
//
//  Created by Apple Apple on 13.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ModelMain.h"
#import "ARCOViewControllerBase.h"

@implementation ARCOViewControllerBase
@synthesize model = _model;




-(void)addEventListener:(NSString *)inevent selector:(SEL)inselector{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:inselector name:inevent object:nil];
}

-(void)removeEventListener:(NSString *)inevent{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:inevent object:nil];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{

    [super viewDidLoad];
    //Get reference to MainModel
    _model = [ModelMain sharedInstance];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    _model = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
