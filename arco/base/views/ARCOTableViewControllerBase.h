//
//  TableViewControllerBase.h
//  WeatherBar
//
//  Created by Apple Apple on 13.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ModelMain.h"
#import <UIKit/UIKit.h>

@interface ARCOTableViewControllerBase : UITableViewController{
    ModelMain *model;
}

@property (nonatomic, strong) ModelMain *model;
- (void) addEventListener:(NSString*)inevent selector:(SEL)inselector;
- (void) removeEventListener:(NSString*)inevent;

@end
