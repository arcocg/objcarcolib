//
//  TDViewControllerBase.m
//
//
//  Created by Artem Shu on 11/30/12.
//  Copyright (c) 2012 Artem Shu. All rights reserved.
//

#import "ARCOViewControllerBaseExt.h"
#import "ARCOModelBaseExt.h"

@interface ARCOViewControllerBaseExt ()
-(void) hideErrorView;
-(void) showErrorView;
@end

@implementation ARCOViewControllerBaseExt

////////////////////////////////////////////////////////////////////////
#pragma mark - Init stuff
////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//----------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self build];
    if ([self isDataExists]) [self dataDidLoaded];
    else [self showPreloader];
//    [self addEventListener:EVENT_MODEL_LANG_CHANGED :@selector(onModel_langChanged:)];
//    [self updateLanguage];

}

//----------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self isDataExists]) [self hidePreloader];
    else [self dataUpdate];
  
    //[self updateLanguage];
}

//----------------------------------------------------------------------
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if ([self isDataExists]) [self hidePreloader];
    [self unregisterHanlders];
}

//----------------------------------------------------------------------
-(void)build{
    //Place all init suff in children here.
}


////////////////////////////////////////////////////////////////////////
#pragma mark - Buttons handlers
////////////////////////////////////////////////////////////////////////
-(void)onBtnRetryPressed:(id)sender{
    [self hideErrorView];
    [self dataUpdate];
}

////////////////////////////////////////////////////////////////////////
#pragma mark - Event handlers
////////////////////////////////////////////////////////////////////////
/*-(void) onModel_langChanged:(id)sender{
    [self updateLanguage];
}*/


////////////////////////////////////////////////////////////////////////
#pragma mark - Data loading processing
////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------
-(void) onModelLoadData_error:(NSNotification*)notification{
    [self dataDidFailed];
}

//----------------------------------------------------------------------
-(void) onModelLoadData_success:(NSNotification*)notification{
    if ([self isDataExists]) [self dataDidLoaded];
}

//----------------------------------------------------------------------
/**
 Checks if model has enough data to display view
 @returns  YES if data to dispay view exists in model, NO - otherwise
 */
-(BOOL) isDataExists{
    return YES;
}

//----------------------------------------------------------------------
-(void)dataDidFailed{
    [self unregisterHanlders];
    [self hidePreloader];
    [self showErrorView];
}

-(void)dataDidLoaded{
    [self unregisterHanlders];
    [self hidePreloader];
    [self updateLanguage];
}

//----------------------------------------------------------------------
/**
 Data needed for view display is updating in model
 */
-(void) dataUpdate{
    
    if ([self isDataExists]) return;
    [self registerHanlders];
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        if (![self isDataExists])
//            [self showPreloader];
//    });
    [self hideErrorView];
}


////////////////////////////////////////////////////////////////////////
#pragma mark - Displaying
////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------
-(void)hideErrorView{
    if (vwLoadError){
        vwLoadError.hidden = YES;
        /*[vwLoadError removeFromSuperview];
        vwLoadError = nil;*/
    }
}

//----------------------------------------------------------------------
-(void)hidePreloader{
//    [MBProgressHUD hideHUDForView:self.view animated:NO];
}

//----------------------------------------------------------------------
-(void)showErrorView{
    //Create error view with Retry button, if not created yet
    /*if (!vwLoadError){
        
        vwLoadError = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        vwLoadError.backgroundColor = [UIColor whiteColor];
        vwLoadError.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height/2 - 30, vwLoadError.frame.size.width, 50)];
        //lbl.center = CGPointMake(self.view.frame.size.width/2, 25);
        lbl.font = [UIFont fontWithName:@"Helvetica" size:30];
        lbl.numberOfLines = 0;
        lbl.lineBreakMode = NSLineBreakByWordWrapping;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = [self langString:@"ConnectionError"];
        lbl.textColor = [UIColor blackColor];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin|
                                UIViewAutoresizingFlexibleRightMargin|
                                UIViewAutoresizingFlexibleTopMargin|
                                UIViewAutoresizingFlexibleBottomMargin;
        lbl.tag = 101;
        [vwLoadError addSubview:lbl];
        
        //Create "Retry" button
        UIButton *btnretry = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 154, self.view.frame.size.height/2 + 30, 308, 54)];
        [btnretry setBackgroundImage:[[UIImage imageNamed:@"Button.Yellow.BG.png"] stretchableImageWithLeftCapWidth:7 topCapHeight:0] forState:UIControlStateNormal];
        [btnretry setTitle:[self langString:@"TryAgain"] forState:UIControlStateNormal];
        btnretry.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:19];
        [btnretry setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnretry setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btnretry addTarget:self action:@selector(onBtnRetryPressed:) forControlEvents:UIControlEventTouchUpInside];
        btnretry.tag = 102;
        btnretry.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin|
                                UIViewAutoresizingFlexibleRightMargin|
                                UIViewAutoresizingFlexibleTopMargin|
                                UIViewAutoresizingFlexibleBottomMargin;
        [vwLoadError addSubview:btnretry];
        [self.view addSubview:vwLoadError];
    }
    
    vwLoadError.hidden = NO;
    vwLoadError.alpha = 0;
    [TweenC fadeView:vwLoadError toAlpha:1 withAnimation:kTweenEaseOutSine withDuration:1 delegate:self];*/
}

//----------------------------------------------------------------------
-(void)showPreloader{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //[MBProgressHUD HUDForView:self.view].dimBackground = YES;
}

//----------------------------------------------------------------------
-(void)registerHanlders{
    [self addEventListener:(NSString*)EVENT_RESPONSE_ERROR selector:@selector(onModelLoadData_error:) ];
    [self addEventListener:(NSString*)EVENT_RESPONSE_SUCCESS selector:@selector(onModelLoadData_success:) ];
}

//----------------------------------------------------------------------
-(void)unregisterHanlders{
    [self removeEventListener:(NSString*)EVENT_RESPONSE_ERROR];
    [self removeEventListener:(NSString*)EVENT_RESPONSE_SUCCESS];
}


////////////////////////////////////////////////////////////////////////
#pragma mark - Language stuff
////////////////////////////////////////////////////////////////////////
/*-(NSString *)langString:(NSString *)inkey{
    return  [self.model getCurLanguageString:inkey];
}*/

-(void)updateLanguage{
//    if (vwLoadError){
//        UILabel *lbl = (UILabel*)[vwLoadError viewWithTag:101];
//        lbl.text = lbl.text = [self langString:@"ConnectionError"];
//        
//        UIButton* btnretry = (UIButton*)[vwLoadError viewWithTag:102];
//        [btnretry setTitle:[self langString:@"TryAgain"] forState:UIControlStateNormal];
//    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - View stuff
////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------
-(void)resize:(BOOL)inanimate{
}

//----------------------------------------------------------------------
-(void)resize:(BOOL)inanimate isPortrait:(BOOL)isPortrait screenWidth:(int)screenWidth screenHeight:(int)screenHeight{
}

//----------------------------------------------------------------------
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

//----------------------------------------------------------------------
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resize:NO];
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    BOOL isPortrait = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]);
    [self resize:NO isPortrait:isPortrait
     screenWidth:isPortrait?screenRect.size.width:screenRect.size.height
    screenHeight:isPortrait?screenRect.size.height:screenRect.size.width];
    
    if (!vwLoadError){
        
    }
}
@end
