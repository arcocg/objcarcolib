//
//  TableViewControllerBase.m
//  WeatherBar
//
//  Created by Apple Apple on 13.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ARCOTableViewControllerBase.h"


@implementation ARCOTableViewControllerBase
@synthesize model = _model;


-(void)addEventListener:(NSString *)inevent selector:(SEL)inselector{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:inselector name:inevent object:nil];
}

-(void)removeEventListener:(NSString *)inevent{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:inevent object:nil];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Get reference to MainModel
    _model = [ModelMain sharedInstance];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self setModel:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
