//
//  ARTabBarViewControllerBase.m
//  Technodom
//
//  Created by Artem Shu on 2/28/13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "ARCOTabBarViewControllerBase.h"

@interface ARCOTabBarViewControllerBase ()

@end

@implementation ARCOTabBarViewControllerBase

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //Get reference to MainModel
    _model = [ModelMain sharedInstance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addEventListener:(NSString *)inevent selector:(SEL)inselector{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:inselector name:inevent object:nil];
}

-(void)removeEventListener:(NSString *)inevent{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:inevent object:nil];
}

@end
