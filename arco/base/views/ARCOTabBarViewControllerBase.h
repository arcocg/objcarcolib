//
//  ARTabBarViewControllerBase.h
//  Technodom
//
//  Created by Artem Shu on 2/28/13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "AppDelegate.h"
#import "ModelMain.h"
#import <UIKit/UIKit.h>

@interface ARCOTabBarViewControllerBase : UITabBarController

@property (nonatomic, strong) ModelMain *model;

- (void) addEventListener:(NSString*)inevent selector:(SEL)inselector;
- (void) removeEventListener:(NSString*)inevent;

@end
