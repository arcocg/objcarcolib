//
//  ViewBase.h
//  ArcoObjCLib
//
//  Created by Apple Apple on 07.02.12.
//  Copyright (c) 2012 arcomailbox@gmail.com. All rights reserved.
//

#import "ModelMain.h"
#import <UIKit/UIKit.h>

@interface ARCOViewBase : UIView

@property (nonatomic, retain) ModelMain *model;
- (id) initWithModel:(ModelMain*)inmodel;
- (id) initWithModel:(ModelMain*)inmodel frame:(CGRect)inframe;
- (void) addEventListener:(NSString*)inevent selector:(SEL)inselector;
- (void) buildWithModel:(ModelMain*)inmodel;
- (void) removeEventListener:(NSString*)inevent;
- (void) reorderToPortrait:(CGRect)inframe;
- (void) reorderToLandscape:(CGRect)inframe;
- (void) changeLayout:(CGRect)inframe;
@end
