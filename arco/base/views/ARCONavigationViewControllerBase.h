//
//  NavigationViewControllerBase.h
//  Technodom
//
//  Created by Apple on 30.01.13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "AppDelegate.h"
#import "ModelMain.h"
#import <UIKit/UIKit.h>

@interface ARCONavigationViewControllerBase : UINavigationController{
    ModelMain *model;
}

@property (nonatomic, strong) ModelMain *model;

- (void) addEventListener:(NSString*)inevent selector:(SEL)inselector;
- (void) removeEventListener:(NSString*)inevent;

@end
