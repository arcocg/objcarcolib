//
//  NavigationViewControllerBase.m
//  Technodom
//
//  Created by Apple on 30.01.13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "ARCONavigationViewControllerBase.h"

@interface ARCONavigationViewControllerBase ()

@end

@implementation ARCONavigationViewControllerBase
@synthesize model = _model;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    //Get reference to MainModel
    _model = [ModelMain sharedInstance];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    _model = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addEventListener:(NSString *)inevent selector:(SEL)inselector{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:inselector name:inevent object:nil];
}

-(void)removeEventListener:(NSString *)inevent{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:inevent object:nil];
}

@end
