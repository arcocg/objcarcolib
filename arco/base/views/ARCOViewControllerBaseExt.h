//
//  TDViewControllerBase.h
//  
//
//  Created by Artem Shu on 11/30/12.
//  Copyright (c) 2012 Artem Shu. All rights reserved.
//

#import "ModelMain.h"
//#import "MBProgressHUD.h"
//#import "TweenC.h"
#import "ARCOViewControllerBase.h"
#import <UIKit/UIKit.h>

@interface ARCOViewControllerBaseExt : ARCOViewControllerBase/*<MBProgressHUDDelegate>*/{
    UIView* vwLoadError;
}

/*! Init */
-(void)build;

//Interaction with server data
- (BOOL) isDataExists;          //Is data for view exists in Model or data loading is needed
- (void) dataUpdate;            //Update data needed for view
- (void) dataDidLoaded;         //Data successful loading handle
- (void) dataDidFailed;         //Data loading error handle
- (void) hidePreloader;         //Hide data loading activity indicator
- (void) showPreloader;         //Show data loading activity indicator
- (void) registerHanlders;      //Register data loading handles
- (void) unregisterHanlders;    //Unregister data loading handles

- (void) resize:(BOOL)inanimate;
- (void) resize:(BOOL)inanimate isPortrait:(BOOL)isPortrait screenWidth:(int)screenWidth screenHeight:(int)screenHeight;

//Language stuff
//- (NSString *) langString:(NSString *)inkey;
- (void) updateLanguage;


@property (nonatomic, assign) BOOL isViewDisapear;
@end
