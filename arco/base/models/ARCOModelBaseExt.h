//
//  ARCOModelBaseExt.h
//  ASDC
//
//  Created by Artem Shu on 09.04.13.
//  Copyright (c) 2013 KMG. All rights reserved.
//

#import "ARCOModelBase.h"

extern NSString* const EVENT_RESPONSE_ERROR;
extern NSString* const EVENT_RESPONSE_SUCCESS;

@interface ARCOModelBaseExt : ARCOModelBase

@end
