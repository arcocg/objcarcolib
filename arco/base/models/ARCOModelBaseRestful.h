//
//  ModelBaseRestful.h
//  TechnodomApp
//
//  Created by Artem Shu on 11/22/12.
//  Copyright (c) 2012 Artem Shu. All rights reserved.
//

#import "ARCOModelBase.h"
//#import "SBJsonParser.h"
#import <RestKit/RestKit.h>
#import <RestKit/RKJSONParserJSONKit.h>

@interface ARCOModelBaseRestful : ModelBase{
    //Requests
    NSMutableArray *requestQueue;
}


-(void) processRequestedData:(NSMutableDictionary*)indata requestType:(int)inrequestType;

//Requests queue
- (void) keepRequestAtQueueWithType:(int)intype paramsOrNil:(NSMutableDictionary*)inparams userdataOrNil:(NSMutableDictionary*) inuserdata;
- (void) restoreRequestFromQueue;

@end
