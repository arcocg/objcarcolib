//
//  ModelBase.h
//  WeatherBar
//
//  Created by Apple Apple on 07.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARCOModelBase : NSObject

extern NSString * const EVENT_MODEL_INITED;
extern NSString * const EVENT_STATE_CHANGED;

@property (nonatomic, assign) bool fModelInited;
@property (nonatomic, assign) uint state;

-(void)dispatch:(NSString*)event object:(id)object;
-(void)modelInited;
@end
