//
//  ARCOModelBaseExt.m
//  ASDC
//
//  Created by Artem Shu on 09.04.13.
//  Copyright (c) 2013 KMG. All rights reserved.
//

#import "ARCOModelBaseExt.h"

@implementation ARCOModelBaseExt
NSString * const EVENT_RESPONSE_ERROR = @"eventResponseError";
NSString * const EVENT_RESPONSE_SUCCESS = @"eventResponseSuccess";

@end
