//
//  ModelBase.m
//
//  Created by arcocg@gmail.com 07.02.12.
//  Copyright (c) 2012 Arco. All rights reserved.
//

#import "ARCOModelBase.h"

@implementation ARCOModelBase

@synthesize fModelInited;
@synthesize state = _state;

NSString * const EVENT_MODEL_INITED = @"eventModelInited";
NSString * const EVENT_STATE_CHANGED = @"eventModelStateChanged";

- (id)init {
    self = [super init];
    if (self) {
        self.fModelInited = false;
    }
    return self;
}

-(void)dispatch:(NSString*)event object:(id)object{
    [[NSNotificationCenter defaultCenter] postNotificationName:event object:object];    
}


-(void)modelInited
{
    self.fModelInited = true;
    [self dispatch:EVENT_MODEL_INITED object:nil];
}

- (void)setState:(uint)instate{
    if (_state!=instate){
        uint oldstate = _state;
        _state = instate;
        NSDictionary *obj = @{@"state":[NSNumber numberWithUnsignedInt:_state], @"old":[NSNumber numberWithUnsignedInt:oldstate]  };
        [self dispatch:EVENT_STATE_CHANGED object:obj];
    }
}
@end
