//
//  ModelBaseRestful.m
//  TechnodomApp
//
//  Created by Artem Shu on 11/22/12.
//  Copyright (c) 2012 Artem Shu. All rights reserved.
//

#import "ARCOModelBaseRestful.h"

@implementation ARCOModelBaseRestful


/**
 * Do request to server with params. Could be ovveriden to add specific params to requests by type.
 @param intype Type of request. Could be defined in enum.
 @returns  Request object
 */
-(RKRequest *)doRequest:(int)intype :(NSMutableDictionary*)inparams:(NSMutableDictionary*)inuserdata{
    
    NSLog(@"[ModelBaseRestful] doRequest: intype = %i", intype);
    
    if (!inparams) inparams = [[NSMutableDictionary alloc] init];
    
    /*switch (intype) {
        case REQUEST_CATALOG:
        {
            [inparams setValue:@"cat" forKey:@"module"];
            //[inparams setValue:@"1" forKey:@"cat"];
        }
       
        default:
        break;
    }*/
    
    NSLog(@"[ModelBaseRestful] doRequest: inparams = %@", inparams);
    
    RKRequest* req = [[RKClient sharedClient] get:@"/api.php" queryParameters:inparams delegate:self];
    
    //Set request type in user data
    if (!inuserdata) inuserdata = [[NSMutableDictionary alloc] init];
    [inuserdata setValue:[NSString stringWithFormat:@"%i",intype] forKey:@"type"];
    req.userData  = inuserdata;

    return req;
}


/**
 * Should be overriden to process pesponses data from server
 @param indata      Parsed data goten from server
 @param requestType Type of request data comes for
 */
-(void)processRequestedData:(NSMutableDictionary *)indata requestType:(int)inrequestType{
        
}

////////////////////////////////////////////////////////////////////////
#pragma mark - Requests Queue
////////////////////////////////////////////////////////////////////////

/**
 * Keeps request in queue to execute it after current in processing
 */
-(void)keepRequestAtQueueWithType:(int)intype paramsOrNil:(NSMutableDictionary *)inparams userdataOrNil:(NSMutableDictionary *)inuserdata{
    NSMutableDictionary *req = [[NSMutableDictionary alloc] init];
    [req setValue:[NSString stringWithFormat:@"%i", intype] forKey:@"type"];
    if (inparams!=nil) [req setValue:inparams forKey:@"params"];
    if (inuserdata!=nil) [req setValue:inuserdata forKey:@"userdata"];
    
    [requestQueue addObject:req];
}

/**
 * Restore saved request from queue by FIFO and send it to server
 */
-(void)restoreRequestFromQueue{
    if (requestQueue.count == 0) return;
    
    NSMutableDictionary *req = [requestQueue objectAtIndex:0];
    [requestQueue removeObjectAtIndex:0];
    [self doRequest:[[req objectForKey:@"type"] intValue] :[req objectForKey:@"params"] :[req objectForKey:@"userdata"]];
}


////////////////////////////////////////////////////////////////////////
#pragma mark - RKRequestDelegate
////////////////////////////////////////////////////////////////////////
/**
 Server successful responses processing
 */
- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    NSMutableDictionary *userdata = request.userData;
    
    //Parse JSON
    //SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    RKJSONParserJSONKit* jsonParser = [RKJSONParserJSONKit new];
    NSMutableDictionary* _jsonData =  [jsonParser objectFromString:[[NSString alloc] initWithData:response.body  encoding:NSUTF8StringEncoding] error:nil];
    
   // NSMutableDictionary* _jsonData = [jsonParser objectWithData:response.body];
    NSLog(@"[ModelData] didLoadResponse: _jsonData = %@", _jsonData);
    
    [self processRequestedData:_jsonData requestType:[[request.userData objectForKey:@"type"] intValue]];
    
}

@end
