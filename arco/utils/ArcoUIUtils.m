//
//  ArcoUIUtils.m
//  Arco
//
//  Created by Artem Shu on 10/1/12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import "ArcoUIUtils.h"

@implementation ARCOUIUtils


+ (UIScrollView *)getScrollViewFromWebView: (UIWebView*) webView {
    UIScrollView *scrollView = nil;
    
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending) {
        return webView.scrollView;
    }
    else {
        for (UIView *subview in [webView subviews]) {
            if ([subview isKindOfClass:[UIScrollView class]]) {
                scrollView = (UIScrollView *)subview;
            }
        }
        
        if (scrollView == nil) {
            NSLog(@"Couldn’t get default scrollview!");
        }
    }
    return scrollView;
}
@end
