//
//  ARCOStrinUtils.m
//  Technodom
//
//  Created by Apple on 15.02.13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "ARCOStringUtils.h"

@implementation ARCOStringUtils


/**
 * Escapes characters. Useful for url character escaping
 @param instring Unescaped string
 @returns  Escaped string
 */
+(NSString *)escapedStringWithString:(NSString *)instring{
    NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                    NULL,
                                    (__bridge CFStringRef) instring,
                                    NULL,
                                    CFSTR("!*'();:@&=+$,/?%#[]"),
                                    kCFStringEncodingUTF8));
    return escapedString;
}
@end
