//
//  ArcoCommonUtils.h
//  iPoint
//
//  Created by Artem Shu on 10/4/12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARCOCommonUtils : NSObject

+(NSString*) nounsWithNum:(uint)num noun1:(NSString*)s1 noun2:(NSString*)s2 noun3:(NSString*)s3;
+(NSString*) nounsFromArrayWithNum:(uint)num  arrayWithNouns:(NSArray*)inarr;

@end
