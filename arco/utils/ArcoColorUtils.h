//
//  ColorUtils.h
//  iPoint
//
//  Created by Apple on 10.07.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef  endColor);

@interface ARCOColorUtils : NSObject

+ (UIColor*) UIColorFromRGB:(uint)rgbValue;
+(UIColor*) UIColorFromRGBWithAlpha:(uint)rgbValue alpha:(float) a;
+ (UIImage *)imageWithColorFill:(UIImage *)origImage withColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)insize;
+ (UIImage *)imageWithRoundedCorners:(UIImage *) image radius:(float)radius;
+ (UIImage *)stretchableRoundedImageWithColor:(UIColor*)incolor radius:(float)radius;
@end

@interface UIImage (Tint)
- (UIImage *)imageTinted:(UIColor *)tintColor;
@end