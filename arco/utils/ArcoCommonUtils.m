//
//  ArcoCommonUtils.m
//  iPoint
//
//  Created by Artem Shu on 10/4/12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import "ARCOCommonUtils.h"

@implementation ARCOCommonUtils
+(NSString*) nounsWithNum:(uint)num noun1:(NSString*)s1 noun2:(NSString*)s2 noun3:(NSString*)s3
{
    NSString* num_str = [NSString stringWithFormat:@"%i", num];
    if (num == 0) return s3;
    else if (num != 11 && [num_str hasSuffix:@"1"]) return s1;
    else if (num < 5) return s2;
    else if (num == 11) return s3;
    else return s3;
    return @"";
}


+(NSString*) nounsFromArrayWithNum:(uint)num  arrayWithNouns:(NSArray*)inarr
{
    return [ARCOCommonUtils nounsWithNum:num noun1:[inarr objectAtIndex:0] noun2:[inarr objectAtIndex:1] noun3:[inarr objectAtIndex:2]];
}


@end
