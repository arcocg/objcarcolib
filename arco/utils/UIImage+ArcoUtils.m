//
//  UIImage+ArcoUtils.m
//  ArcoUtils
//
//  Created by Artem Shu on 04.01.15.
//  Copyright (c) 2015 MintApps. All rights reserved.
//

#import "UIImage+ArcoUtils.h"


//TODO: Move to other file
/**
 Create UIColor from hex value. UIColorFromRGBWithAlpha:0xCECECE
 @param rgbValue Hex value of color. 0xCECECE for example
 @returns  UIColor object
 */
UIColor* UIColorFromRGB(uint rgbValue){
    return ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0
                            green:((float)((rgbValue & 0xFF00) >> 8))/255.0
                             blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]);
    
}

/**
 Create UIColor from hex value and alpha. UIColorFromRGBWithAlpha:0xCECECE: 0.8
 @param rgbValue Hex value of color. 0xCECECE for example
 @param a Alpha from 0.0 to 1.0
 @returns  UIColor object
 */
UIColor* UIColorFromRGBWithAlpha(uint rgbValue, float a) {
    return ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0
                            green:((float)((rgbValue & 0xFF00) >> 8))/255.0
                             blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]);
    
}

@implementation UIImage (ArcoColor)

//----------------------------------------------------------------------
+ (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].scale, [UIScreen mainScreen].scale);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


//----------------------------------------------------------------------
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)insize{
    CGRect rect = CGRectMake(0.0f, 0.0f, insize.width/**[UIScreen mainScreen].scale*/, insize.height/**[UIScreen mainScreen].scale*/);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

//----------------------------------------------------------------------
- (UIImage *)imageWithColorFill:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, self.size.width*[UIScreen mainScreen].scale, self.size.height*[UIScreen mainScreen].scale);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, self.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage
                                                scale:[UIScreen mainScreen].scale orientation: UIImageOrientationDownMirrored];
    
    return flippedImage;
}


//----------------------------------------------------------------------
- (UIImage *)imageWithRoundedCorners:(float)radius
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    imageLayer.contents = (id) self.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}



//----------------------------------------------------------------------
- (UIImage*)stretchableRoundedImageWithColor:(UIColor*)incolor radius:(float)radius{
    UIImage* img = [UIImage imageWithColor:incolor size:CGSizeMake(radius*2+1, radius*2+1)];
    return  [[img imageWithRoundedCorners:radius] stretchableImageWithLeftCapWidth:radius topCapHeight:radius];
    
}

@end
