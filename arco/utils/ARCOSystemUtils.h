//
//  ARCOSystemUtils.h
//  Arco
//
//  Created by Artem Shu on 24.03.13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARCOSystemUtils : NSObject

+ (NSString*)retrieveFromUserDefaults:(NSString*)key;
+ (void)saveToUserDefaults:(NSString*)key value:(NSString*)valueString;

@end
