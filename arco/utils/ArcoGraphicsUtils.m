//
//  ARCOGraphicsUtils.m
//  iPoint
//
//  Created by Apple on 11.08.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import "ARCOGraphicsUtils.h"

void CGContextAddRoundRect(CGContextRef incontext, CGRect rect, float radius)
{
	CGContextRef context = incontext;
	[[UIColor whiteColor] set];
	CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
	CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
	CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius, 
                    radius, M_PI / 4, M_PI / 2, 1);
	CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius, 
                            rect.origin.y + rect.size.height);
	CGContextAddArc(context, rect.origin.x + rect.size.width - radius, 
                    rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
	CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
	CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius, 
                    radius, 0.0f, -M_PI / 2, 1);
	CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
	CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius, 
                    -M_PI / 2, M_PI, 1);
    
}

CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius) {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect), 
                        CGRectGetMaxX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect), 
                        CGRectGetMinX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect), 
                        CGRectGetMinX(rect), CGRectGetMinY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect), 
                        CGRectGetMaxX(rect), CGRectGetMinY(rect), radius);
    CGPathCloseSubpath(path);
    
    return path;        
}

UIImage* rotateImage(UIImage *image, float degrees){
    CGFloat rads = M_PI * degrees / 180;
    
    float screenScale = [UIScreen mainScreen].scale;
    float w = [image size].width;
    float h = [image size].height;
    float newSide = MAX(w*screenScale, w*screenScale);
    UIImage* resultImage;
    
    CGSize size =  CGSizeMake(newSide, newSide);
    UIGraphicsBeginImageContext(size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, newSide/2, newSide/2);
    CGContextRotateCTM(ctx, rads);
    CGContextDrawImage(UIGraphicsGetCurrentContext(),
                       CGRectMake((-w*cos(rads) -h*sin(rads))/2*screenScale,
                                  (-h*cos(rads) -w*sin(rads))/2*screenScale,
                                  size.width,
                                  size.height),
                       image.CGImage);
    
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    resultImage = [[UIImage alloc] initWithCGImage:cgImage scale:screenScale orientation:UIImageOrientationDown];
    
    CGImageRelease(cgImage);
    //CGContextRelease(ctx);
    UIGraphicsEndImageContext();
    return resultImage;
}

@implementation ARCOGraphicsUtils

@end
