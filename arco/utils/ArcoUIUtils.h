//
//  ArcoUIUtils.h
//  Arco
//
//  Created by Artem Shu on 10/1/12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARCOUIUtils : NSObject

+ (UIScrollView *)getScrollViewFromWebView: (UIWebView*) webView;

@end
