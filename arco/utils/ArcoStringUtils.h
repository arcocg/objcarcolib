//
//  ARCOStrinUtils.h
//  Technodom
//
//  Created by Apple on 15.02.13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARCOStringUtils : NSObject
+(NSString*)escapedStringWithString:(NSString*)instring;
@end
