//
//  ArcoGraphicsUtils.h
//  iPoint
//
//  Created by Apple on 11.08.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius);
void CGContextAddRoundRect(CGContextRef incontext, CGRect rect, float radius);
UIImage* rotateImage(UIImage *image, float degrees);

@interface ARCOGraphicsUtils : NSObject

@end
