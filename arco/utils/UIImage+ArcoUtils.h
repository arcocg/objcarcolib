//
//  UIImage+ArcoUtils.h
//  ArcoUtils
//
//  Created by Artem Shu on 04.01.15.
//  Copyright (c) 2015 MintApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ArcoUtils)
/**
 * Generate UImage 1x1 or 2x2 for retina filled with color
 @param color Color to fill image with
 @returns  Image filled with solid color
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 * Generate UImage with needed size. For Retina sizes converts automaticaly
 @param color Color to fill image with
 @returns  Image filled with solid color
 */

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)insize;

/**
 Fills transparent image with color
 @param color Color to fill with
 @returns  Colored image
 */
- (UIImage *)imageWithColorFill:(UIColor *)color;

/**
 * Clip image to mask with rounded corners, generates new umage and return it
 @param radius Needed corner radius
 @returns  New image with rounded corners
 */
- (UIImage *)imageWithRoundedCorners:(float)radius;

/**
 * Generate stretchable quad image with radius-corners and filled it with incolor-color
 @param incolor Hex color to fill image with
 @param radius  Radius of corners
 @returns  Stretchable color-filled image with fixed corners
 */
- (UIImage *)stretchableRoundedImageWithColor:(UIColor*)incolor radius:(float)radius;
@end
