//
//  ColorUtils.m
//  iPoint
//
//  Created by Apple on 10.07.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import "ARCOColorUtils.h"

void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, 
                        CGColorRef  endColor) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, 
                                                        (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace); 
}


@implementation ARCOColorUtils

/**
 Create UIColor from hex value. UIColorFromRGBWithAlpha:0xCECECE
 @param rgbValue Hex value of color. 0xCECECE for example
 @returns  UIColor object
 */
+ (UIColor*) UIColorFromRGB:(uint)rgbValue{
   return ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 
                    green:((float)((rgbValue & 0xFF00) >> 8))/255.0 
                    blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]);
    
}

/**
 Create UIColor from hex value and alpha. UIColorFromRGBWithAlpha:0xCECECE: 0.8
 @param rgbValue Hex value of color. 0xCECECE for example
 @param a Alpha from 0.0 to 1.0
 @returns  UIColor object
 */
+ (UIColor*) UIColorFromRGBWithAlpha:(uint)rgbValue alpha:(float) a {
    return ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 
             green:((float)((rgbValue & 0xFF00) >> 8))/255.0 
             blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]);
    
}


/**
 * Generate UImage 1x1 or 2x2 for retina filled with color
 @param color Color to fill image with
 @returns  Image filled with solid color
 */
+ (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].scale, [UIScreen mainScreen].scale);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


/**
 * Generate UImage with needed size. For Retina sizes converts automaticaly
 @param color Color to fill image with
 @returns  Image filled with solid color
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)insize{
    CGRect rect = CGRectMake(0.0f, 0.0f, insize.width*[UIScreen mainScreen].scale, insize.height*[UIScreen mainScreen].scale);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/**
 Fills transparent image with color
 @param origImage Original image
 @param origImage Color to fill with
 @returns  Colored image
 */
+ (UIImage *)imageWithColorFill:(UIImage *)origImage withColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, origImage.size.width*[UIScreen mainScreen].scale, origImage.size.height*[UIScreen mainScreen].scale);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, origImage.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage
                                               scale:[UIScreen mainScreen].scale orientation: UIImageOrientationDownMirrored];
    
    return flippedImage;
}


/**
 * Clip image to mask with rounded corners, generates new umage and return it
 @param image  Source image
 @param radius Needed corner radius
 @returns  New image with rounded corners
 */
+(UIImage *)imageWithRoundedCorners:(UIImage *) image radius:(float)radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [UIScreen mainScreen].scale);;
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}



/**
 * Generate stretchable quad image with radius-corners and filled it with incolor-color
 @param incolor Hex color to fill image with
 @param radius  Radius of corners
 @returns  Stretchable color-filled image with fixed corners
 */
+(UIImage*)stretchableRoundedImageWithColor:(UIColor*)incolor radius:(float)radius{
    UIImage* img = [ARCOColorUtils imageWithColor:incolor size:CGSizeMake(radius*2+1, radius*2+1)];
    return  [[ARCOColorUtils imageWithRoundedCorners:img radius:radius]
             resizableImageWithCapInsets:UIEdgeInsetsMake(radius, radius, radius, radius)];
    
}


@end

@implementation UIImage (Tint)
- (UIImage *)imageTinted:(UIColor *)tintColor {
    UIGraphicsBeginImageContext(self.size);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}
@end
