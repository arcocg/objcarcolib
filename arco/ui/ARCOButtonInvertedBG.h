//
//  ASDCButtonInvertedBG.h
//  ASDC
//
//  Created by Artem Shu on 16.09.13.
//  Copyright (c) 2013 KMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARCOButtonInvertedBG : UIButton

-(void)build;
@end
