//
//  TouchableView.m
//  iPoint
//
//  Created by Apple on 23.07.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import "ARCOTouchableView.h"

@implementation ARCOTouchableView
@synthesize delegate = _delegate;

- (id)init {
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
    }
    return self;
}



-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [_delegate touchableViewTouch:self];
}
@end
