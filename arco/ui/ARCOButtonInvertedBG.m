//
//  ASDCButtonInvertedBG.m
//  ASDC
//
//  Created by Artem Shu on 16.09.13.
//  Copyright (c) 2013 KMG. All rights reserved.
//

#import "ARCOButtonInvertedBG.h"

@implementation ARCOButtonInvertedBG

- (id)init
{
    self = [super init];
    if (self) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self build];
        });
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self build];
        });
    }
    return self;
}

-(void)build{
    UIImage *bg_img = [self backgroundImageForState:UIControlStateNormal];
    UIImage *flippedImage = [UIImage imageWithCGImage:bg_img.CGImage
                                                scale:[UIScreen mainScreen].scale orientation: UIImageOrientationDownMirrored];
    [self setBackgroundImage:flippedImage forState:UIControlStateSelected];
    [self setBackgroundImage:flippedImage forState:UIControlStateHighlighted];
    [self setBackgroundImage:flippedImage forState:UIControlStateHighlighted|UIControlStateSelected];
    
}
@end
