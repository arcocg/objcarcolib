//
//  UILabelUnderlined.m
//  Technodom
//
//  Created by Artem Shu on 1/17/13.
//  Copyright (c) 2013 Artem Shu. All rights reserved.
//

#import "ARCOUILabelUnderlined.h"

@implementation ARCOUILabelUnderlined

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 CGContextRef ctx = UIGraphicsGetCurrentContext();
 CGContextSetRGBStrokeColor(ctx, 207.0f/255.0f, 91.0f/255.0f, 44.0f/255.0f, 1.0f); // RGBA
 CGContextSetLineWidth(ctx, 1.0f);
 
 CGContextMoveToPoint(ctx, 0, self.bounds.size.height - 1);
 CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height - 1);
 
 CGContextStrokePath(ctx);
 
 [super drawRect:rect];
}

@end
