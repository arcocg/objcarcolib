//
//  TouchableView.h
//  iPoint
//
//  Created by Apple on 23.07.12.
//  Copyright (c) 2012 iPoint. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ARCOTouchableViewDelegate
    -(void) touchableViewTouch:(id)sender;
@end


    
@interface ARCOTouchableView : UIView{
    id <ARCOTouchableViewDelegate> delegate;
}

@property (retain, nonatomic) id <ARCOTouchableViewDelegate> delegate;
@end
